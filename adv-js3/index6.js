const stats = {
    age: 20,
    salary: 2000000,
  };
  
  const employee = {
    name: 'Vitalii',
    surname: 'Klichko',
    ...stats,
  };
  
  console.log(employee)