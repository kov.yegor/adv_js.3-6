function get(url, queryParams) {
    const queryParamsString = new URLSearchParams(queryParams);
  
    return fetch(`${url}${!!queryParams ? "?" + queryParamsString : ""}`).then(
      (response) => {
        if (response.ok) {
          return response.json();
        } else {
          return new Error("invalid data");
        }
      }
    );
  }
  
  function getFilms() {
    get("https://ajax.test-danit.com/api/swapi/films").then((filmsArr) => {
      const rootElement = document.querySelector("body");
  
      filmsArr.forEach(({ episodeId, name, openingCrawl, characters }) => {
        const filmList = document.createElement("div");
  
        filmList.insertAdjacentHTML(
          "beforeend",
          `<h2>Epizode ${episodeId}:${name}</h2>
           <p>${openingCrawl}</p>`
        );
  
        rootElement.insertAdjacentElement("beforeend", filmList);
        getCharacters(characters, filmList);
      });
    });
  }
  
  function getCharacters(arrCharacters, filmList) {
    const charactersList = document.createElement("ol");
    charactersList.insertAdjacentText("afterbegin", "Characters:");
  
    arrCharacters.forEach((url) => {
      get(url).then(({ name }) => {
        charactersList.insertAdjacentHTML("beforeend", `<li>${name}</li>`);
      });
  
      filmList.insertAdjacentElement("beforeend", charactersList);
    });
  }
  
  getFilms();
  