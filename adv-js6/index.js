async function showAdress (){
    let response = await fetch('https://api.ipify.org/?format=json');
    let calculateIp = await response.json();
    
    let adressResponce = await fetch(`http://ip-api.com/json/${calculateIp.ip}?continent,country,countryCode,region,regionName,city,district&fields=continent,country,region,regionName,city,district`);

    let calculateAdress = await adressResponce.json()
    console.log(calculateAdress)

    let {continent,country,region,regionName,city,district} = calculateAdress;

      const adress = document.getElementById("adress");
  
        adress.insertAdjacentHTML(
          "afterend",
          `<ul>
          <li>Continent: ${continent}<li>
          <li>Country: ${country}<li>
          <li>Region: ${region}<li>
          <li>Regione name: ${regionName}<li>
          <li>City: ${city}<li>
          <li>District: ${district}<li>
          </ul>`
        );                  
  
}

const calculateBtn = document.getElementById("button");

calculateBtn.addEventListener('click', async (event) =>{showAdress()} );
